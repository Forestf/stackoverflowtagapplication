﻿using StackOverFlowTags.Models;

namespace StackOverFlowTags.ViewModels
{
    public class TagPageViewModel
    {
        public IEnumerable<Tag> TagList { get; set; }
        public Tag Tag => new Tag();
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int ItemsOnPage { get; set; }
    }
}

using StackOverFlowTags;
using StackOverFlowTags.Services;
using StackOverFlowTags.Services.Impl;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpClient<ITagService, TagService>(client =>
{
    client.BaseAddress = new Uri(StackOverflowConstants.StackOverflowApi);
})
.ConfigureHttpMessageHandlerBuilder(builder =>
    {
        builder.PrimaryHandler =
            new HttpClientHandler()
            {
                AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate
            };
    }
);

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

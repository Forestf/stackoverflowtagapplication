﻿using Microsoft.AspNetCore.Mvc;
using StackOverFlowTags.Models;
using StackOverFlowTags.Services;
using StackOverFlowTags.ViewModels;
using System.Diagnostics;

namespace StackOverFlowTags.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ITagService _tagService;
        private readonly int _maxItems = 1000;

        public HomeController(ILogger<HomeController> logger, ITagService tagService)
        {
            _logger = logger;
            _tagService = tagService;
        }

        public async Task<IActionResult> Index(int page = 1, int itemsOnPage = 40)
        {
            var result = await _tagService.GetTagList(page, itemsOnPage);

            TagPageViewModel tagPage = new TagPageViewModel
            {
                CurrentPage = page,
                ItemsOnPage = itemsOnPage,
                TotalPages = _maxItems / itemsOnPage
            };
            if (result != null && result.Items != null)
            {
                int totalCount = result.Items.Sum(x => x.Count);
                foreach (var tag in result.Items)
                    tag.Popularity = Math.Round((double)tag.Count / (double)totalCount * 100d, 2);

                tagPage.TagList = result.Items;
            }
            else
                tagPage.TagList = Enumerable.Empty<Tag>();

            return View(tagPage);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
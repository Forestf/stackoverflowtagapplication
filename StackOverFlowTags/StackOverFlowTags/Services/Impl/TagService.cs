﻿using StackOverFlowTags.Models;
using System.Text.Json;

namespace StackOverFlowTags.Services.Impl
{
    public class TagService : ITagService
    {
        private readonly HttpClient _httpClient;
        public TagService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Tags> GetTagList(int page, int itemsOnPage)
        {

            using (var result = await _httpClient.SendAsync(CreateRequest(page, itemsOnPage)).ConfigureAwait(false))
            {
                using (var contentStream = await result.Content.ReadAsStreamAsync())
                {
                    return await JsonSerializer.DeserializeAsync<Tags>(contentStream);
                }
            }
        }

        private static HttpRequestMessage CreateRequest(int page, int itemsOnPage)
        {
            return new HttpRequestMessage(HttpMethod.Get, $"tags?page={page}&pagesize={itemsOnPage}&order=desc&sort=popular&site=stackoverflow");
        }

    }
}

﻿using StackOverFlowTags.Models;

namespace StackOverFlowTags.Services
{
    public interface ITagService
    {
        Task<Tags> GetTagList(int page, int itemsOnPage);
    }
}

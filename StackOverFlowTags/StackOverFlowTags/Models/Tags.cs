﻿using System.Text.Json.Serialization;

namespace StackOverFlowTags.Models
{
    public class Tags
    {
        [JsonPropertyName("items")]

        public IEnumerable<Tag> Items { get; set; }
    }
}

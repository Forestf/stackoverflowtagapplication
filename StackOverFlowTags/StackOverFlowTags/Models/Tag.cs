﻿using System.Text.Json.Serialization;

namespace StackOverFlowTags.Models
{
    public class Tag
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("count")]
        public int Count { get; set; }
        public double Popularity { get; set; }

    }
}
